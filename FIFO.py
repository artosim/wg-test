# -*- coding: utf-8 -*-
"""
Две реализации: первая с помощью массива, вторая с помощью двух стеков.
Плюсы реализации на двух массивах в том, что очередь может быть неограниченной,
в первой же нам приходится задавать размер массива, что может являться минусом.
В реализации на массиве все операции за O(1), когда в реализации на двух стеках
за амортизированное O(1).
"""
from abc import ABCMeta, abstractmethod


class FIFOBase(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def pushback(self, value):
        pass

    @abstractmethod
    def popfront(self):
        pass

    @abstractmethod
    def isempty(self):
        pass

    @abstractmethod
    def size(self):
        pass


class FIFOarray(FIFOBase):
    def __init__(self, maxsize=10):
        self.maxsize = maxsize
        self._queue = [0] * (self.maxsize + 1)
        self._frontpointer = 0
        self._backpointer = 0

    def _movepointer(self, pointer):
        return (pointer + 1) % (self.maxsize + 1)

    def pushback(self, value):
        nextbackpointer = self._movepointer(self._backpointer)

        if nextbackpointer == self._frontpointer:
            return BufferError

        self._queue[self._backpointer] = value
        self._backpointer = nextbackpointer

    def popfront(self):
        if self.isempty():
            return
        value = self._queue[self._frontpointer]
        self._frontpointer = self._movepointer(self._frontpointer)

        return value

    def isempty(self):
        if self._frontpointer == self._backpointer:
            return True
        return False

    def size(self):
        if self.isempty():
            return 0
        if self._frontpointer < self._backpointer:
            return self._backpointer - self._frontpointer
        else:
            return self.maxsize - self._frontpointer + self._backpointer + 1


class FIFO2stack(FIFOBase):
    def __init__(self):
        self.stack_push = []
        self.stack_pop = []

    def size(self):
        return len(self.stack_pop) + len(self.stack_push)

    def isempty(self):
        if self.size() == 0:
            return True
        return False

    def _swap(self):
        # for _ in range(len(self.stack_push)):
        #     self.stack_pop.append(self.stack_push.pop())
        self.stack_pop = self.stack_push
        self.stack_pop.reverse()
        self.stack_push = []

    def pushback(self, value):
        self.stack_push.append(value)

    def popfront(self):
        if self.isempty():
            return
        if len(self.stack_pop) == 0:
            self._swap()
        return self.stack_pop.pop()
