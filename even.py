# -*- coding: utf-8 -*-
"""
Функция IsEven на порядок быстрее и проще.
"""
from time import time

def myisEven(value):
    return False if value & 1 else True


def IsEven(value):
    return value % 2 == 0

num = 439
start_time = time()
print myisEven(num)
print time() - start_time

start_time = time()
print IsEven(num)
print time() - start_time

# Функция IsEven на порядок быстрее и проще.
