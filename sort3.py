# -*- coding: utf-8 -*-
"""
Quick sort хорошо себя показывает по сравнению с другими алгоритмами
сортировки https://habr.com/ru/post/335920/. Плюсом к этому сортировка
массива проходит inplace. Питону не приходится тратить время на работу
с новыми массивами.
"""
from random import randint


def insertionsort(a, l, r):
    for i in xrange(l, r + 1):
        for j in xrange(i + 1, r + 1):
            if a[j] < a[i]:
                a[j], a[i] = a[i], a[j]
    return


def partition(a, i, j):
    pivot = a[randint(i, j)]
    while i <= j:
        while a[i] < pivot:
            i += 1
        while a[j] > pivot:
            j -= 1
        if i <= j:
            a[i], a[j] = a[j], a[i]
            i += 1
            j -= 1
    return i, j


def quicksort(a, l, r):
    if r <= l:
        return
    if r - l <= 10:
        insertionsort(a, l, r)
    i, j = partition(a, l, r)
    quicksort(a, l, j)
    quicksort(a, i, r)
